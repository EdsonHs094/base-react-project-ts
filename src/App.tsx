import React from 'react';
import LocationList from './components/LocationList';
import AppBar from "@material-ui/core/AppBar";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Toolbar from "@material-ui/core/Toolbar";
import { Grid, Row, Col } from "react-flexbox-grid";
import './App.css';
import { connect } from "react-redux";
import ForecastExtended from "./components/ForecastExtended";
import { store } from "./store";
import { setCity } from "./actions";

const cities = [
  "Lima,pe",
  "Buenos Aires,ar",
  "Bogota,col"
];

interface MyProps {
  dispatchSetCity: Function
}

interface MyState {
  city: any
}

class App extends React.Component<MyProps, MyState> {

  constructor(props:any) {
    super(props);
    this.state = {
      city: null
    }
  }

  handleSelectedLocation = (city:string) => {
    this.setState({
      city
    });
    this.props.dispatchSetCity(city);
  }

  render() {
    const { city } = this.state;
    return (
      <Grid>
          <Row>
            <AppBar position="sticky">
              <Toolbar>
                <Typography variant="h6" color="inherit">
                  Weather App
                </Typography>
              </Toolbar>
            </AppBar>
          </Row>
          <Row>
            <Col xs={12} md={6}>
              <LocationList cities={cities} 
              onSelectedLocation={this.handleSelectedLocation}></LocationList>       
            </Col>
            <Col xs={12} md={6}>
              <Paper>
                <div className="details">
                  {
                    city ?
                    <ForecastExtended city={city}></ForecastExtended> :
                    null
                  }
                </div>
              </Paper>
            </Col>
          </Row>
      </Grid>
    );
  }
}

const mapDispatchToPropsActions = (dispatch:any) => ({
  dispatchSetCity: (value:any) => dispatch(setCity(value))
});
const AppConnected = connect(null, mapDispatchToPropsActions)(App);

export default AppConnected;