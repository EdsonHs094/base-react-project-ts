import React from "react";
import { WeatherData } from "./../WeatherLocation/WeatherData";

interface MyProps {
    weekday: string,
    hour: number,
    data: any
}

const ForecastItem = (props: MyProps) => {
    const {weekday, hour, data} = props;  
    return (
        <div>
            <h2>{weekday} Hora: {hour}</h2>
            <WeatherData data={data}></WeatherData>
        </div>
        );
};

export default ForecastItem;