import React from 'react';
import { Location } from './Location';
import { WeatherData } from './WeatherData';
import transformWeather from './../../services/transformWeather';
import getUrlWeatherByCity from './../../services/getUrlWeatherByCity';
import './styles.css';
import CircularProgress from '@material-ui/core/CircularProgress';

type MyState = {
    city: string
    data: any
}

type MyProps = {
    city: string,
    onWeatherLocationClick: any
}

export class WeatherLocation extends React.Component<MyProps, MyState> {

    constructor(props:any){
        super(props);
        const { city } = props;
        this.state = {
            city,
            data: null
        };
    }

    componentDidMount() {
        this.handleUpdateClick();
    }

    componentDidUpdate(prevProps: any, prevState: any) {
    }

    handleUpdateClick = () => {
        const api_weather = getUrlWeatherByCity(this.state.city);
        fetch(api_weather).then((resolve) => {
            return resolve.json();
        }).then(data => {
            const newWeather = transformWeather(data);
            this.setState({
                data: newWeather
            });
        }); 
    }
    render() {
        const {onWeatherLocationClick} = this.props;
        const {city, data} = this.state;
        return <div className="weatherLocationCont" onClick={onWeatherLocationClick}>
            <Location city={city}></Location>
            {data ? <WeatherData data={data}></WeatherData> : <CircularProgress/>} 
        </div>
    }
};