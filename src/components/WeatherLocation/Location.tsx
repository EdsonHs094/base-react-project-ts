import React from 'react';
import './styles.css';

interface LocationValues {
    city:string
}

export const Location = (props: LocationValues) => {
    const { city } = props
    return <div className="locationCont"><h1> {city} </h1></div>;
};