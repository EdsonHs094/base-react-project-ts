import React from 'react';
import { WeatherExtraInfo } from './WeatherExtraInfo';
import { WeatherTemperature } from './WeatherTemperature';
import './styles.css';


export const WeatherData = ({data: {temperature, weatherState, humidity, wind}}: any) => {
    return <div className="weatherDataCont">
        <WeatherTemperature temperature={temperature} weaterState={weatherState}/>
        <WeatherExtraInfo humidity={humidity} wind={wind}/>
    </div>  
};