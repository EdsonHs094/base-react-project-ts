import React from 'react';
import './styles.css';

interface WeatherExtraInfoValues {
    humidity:number,
    wind:string
}

export const WeatherExtraInfo = (props: WeatherExtraInfoValues) => {
    const {humidity, wind} = props;
    return <div className="weatherExtraInfoCont">
        <span className="extraInfoText">{`Humedad: ${humidity} %`}</span>
        <span className="extraInfoText">{`Vientos: ${wind}`}</span>
    </div>
};