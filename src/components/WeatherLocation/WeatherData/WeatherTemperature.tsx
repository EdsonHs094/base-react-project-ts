    import React from 'react';
import WeatherIcons from 'react-weathericons';
import {
    CLOUD,
    SUN,
    RAIN,
    SNOW, 
    THUNDER, 
    DRIZZLE 
} from './../../../constants/weathers';
import './styles.css';

const icons:any = {
    [CLOUD]: "cloud",
    [SUN]: "day-sunny",
    [RAIN]: "rain",
    [SNOW]: "snow",
    [THUNDER]: "day-thunderstorm",
    [DRIZZLE]: "day-showers"
};

const getWeatherIcon = (weaterState:string) => {
    const icon = (icons[weaterState]) ? icons[weaterState] : "day-sunny";
    const sizeIcon = "4x";
    return <WeatherIcons name={icon} className="wicon" size={sizeIcon}/>;
}

interface TemperatureValues{
    temperature: number,
    weaterState: string
}

export const WeatherTemperature = (props: TemperatureValues) => {
    const {temperature, weaterState} = props;
    return <div className="weatherTemperatureCont">
        {
            getWeatherIcon(weaterState)
        }
        <span className="temperature">{ `${temperature}` }</span>
        <span className="temperatureType">{`C°`}</span>
    </div>
};