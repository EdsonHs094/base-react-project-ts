import React from 'react';
import { WeatherLocation } from "./WeatherLocation";
import './styles.css';

interface LocationListRequest {
    cities: string[],
    onSelectedLocation: any,
}

const LocationList = (props: LocationListRequest) => {
    const {cities, onSelectedLocation} = props;
    const handleWeatherLocationClick = (city:string) => {
        onSelectedLocation(city);
    }
    const strToComponents = (cities: string[]) => (
        cities.map((
            city, index) => 
            <WeatherLocation 
            key={index} 
            city={city} 
            onWeatherLocationClick={() => handleWeatherLocationClick(city)}/>
            )
    );

    return  <div className="locationList"> 
            {strToComponents(cities)}  
        </div>;
};

export default LocationList;