import React from "react";
import ForecastItem from "./ForecastItem";
import transformForecast from "./../services/transformForecast";

interface MyProps {
    city: string
}

interface MyState {
    forecastData: any
}

export const api_key = "4544b1917f1259fe972ccca66941aaac";
export const url_base_weather = "http://api.openweathermap.org/data/2.5/forecast";

class ForecastExtended extends React.Component<MyProps, MyState> {
    constructor(props:any) {
        super(props);
        this.state = {
            forecastData: null
        };
    }

    componentDidMount() {
        this.updateCity(this.props.city)
    }

    componentWillReceiveProps(nextProps:any) {
        if(nextProps.city !== this.props.city) {
            this.setState({
                forecastData: null
            });
            this.updateCity(nextProps.city);
        }
    }

    renderForecastItemDays(forecastData:any) {
        return forecastData.map((forecast:any, index:any) => (
            <ForecastItem key={index} weekday={forecast.weekday} hour={forecast.hour} data={forecast.data}></ForecastItem>
        ));
    };

    renderProgress = () =>
    {
        return <h3>Cargando Pronostico extendido</h3>;
    }

    updateCity(city:string) {
        const url_forecast = `${url_base_weather}?q=${city}&appid=${api_key}`;
        fetch(url_forecast).then(
            data => (data.json())  
        ).then(
            weather_data => {
                const forecastData = transformForecast(weather_data);
                this.setState({
                    forecastData
                });
            }
        );
    }

    render() {
        const { city } = this.props;
        const { forecastData } = this.state;
        return (
            <div>
                <h2 className="forecast-title">Pronostico extendido para {city}</h2>
                {
                    forecastData ? 
                        this.renderForecastItemDays(forecastData) :
                        this.renderProgress()
                }
            </div>
        );
    }
}

export default ForecastExtended;