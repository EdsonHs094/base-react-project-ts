const location = "Lima,pe";
export const api_key = "4544b1917f1259fe972ccca66941aaac";
export const url_base_weather = "http://api.openweathermap.org/data/2.5/weather";

const api_weather = `${url_base_weather}?q=${location}&appid=${api_key}`;

export default api_weather;
